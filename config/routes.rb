Rails.application.routes.draw do
   # API routes path
  namespace :api, defaults: { format: :json } do
    
    get 'teachers/:teacher_id/books/:book_id/text_notes/:sayfaNo' => 'text_notes#show'
    get 'teachers/:teacher_id/books/:book_id/video_notes/:sayfaNo' => 'video_notes#show'
    get 'teachers/:teacher_id/books/:book_id/sound_notes/:sayfaNo' => 'sound_notes#show'  
    get 'students/:student_id/ogrencininogretmeni' => 'students#ogrencininogretmeni'  
    get 'students/:id/ogrencininkitaplari' => 'students#ogrencininkitaplari' 
    post 'students/:student_id/books/:book_id/ensonkalinansayfa'=> 'students_books#ensonkalinansayfa'
    get 'books/:id/kitapicerik' => 'books#kitapicerik'
    get 'students/:student_id/books/:book_id/testistatistikler' => 'students#ogrencininTestSonuclari'
    get 'students/:student_id/books/:book_id/ogrencinincevaplari' => 'students#ogrencininSorularaCevaplari'
    get 'students/:student_id/books/:book_id/ogrencininSecmelicevaplari' => 'students#ogrencininTestSonuclari'
    
    get 'students/:student_id/books/:book_id/kitaptakiDySoruSize'=> 'dogru_yanlis_questions#kitaptakiDySoruSize'
    get 'students/:student_id/books/:book_id/kitaptakiEsSoruSize'=> 'esleme_questions#kitaptakiEsSoruSize'
    get 'students/:student_id/books/:book_id/kitaptakiSecSoruSize'=> 'secmeli_questions#kitaptakiSecSoruSize'

    
    resources :books, :only => [:show,:index] do
    end
    
    resources :students, :only => [:show,:index] do
      collection do
        post 'login'
      end
      resources :teachers, :only => [:show] do
        resources :conversations, :only => [:create] do
        end
      end
      
      resources :conversations, :only => [:index] do
    
      end 
      
      resources :books, :only => [:show] do
        resources :student_chosen_words, :only => [:show, :create, :index, :destroy] do
        end
        resources :students_books, :only => [:show,:index] do
        end
        
      end
    end
  
    resources :conversations, :only => [:index] do
      resources:messages, :only => [:create,:index] do
      
      end
    end 
  
  end
  
  
  namespace :api, defaults: { format: :json } do
    resources :teachers, :only => [:show, :create] do
      resources :books, :only => [:show] do
        resources :teacher_chosen_words, :only => [:show, :index] do
        
        end
      end
    end
  end
  

  namespace :api, defaults: { format: :json } do
    resources :students_dyquestions, :only => [:show,:index] do
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students_esquestions, :only => [:show,:index] do
    end
  end

  namespace :api, defaults: { format: :json } do
    resources :students_secquestions, :only => [:show,:index] do
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :books, :only => [:show] do
        resources :dogru_yanlis_questions, :only => [:show, :create,:index] do
        
        end
      end
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :books, :only => [:show] do
        resources :esleme_questions, :only => [:show, :create,:index] do
        
        end
      end
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :books, :only => [:show] do
        resources :secmeli_questions, :only => [:show, :create,:index] do
        
        end
      end
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :dogru_yanlis_questions, :only => [:show] do
        resources :students_dyquestions, :only => [:show, :create,:index] do
        end
      end
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :esleme_questions, :only => [:show] do
        resources :students_esquestions, :only => [:create] do
        end
      end
    end
  end

  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :secmeli_questions, :only => [:show] do
        resources :students_secquestions, :only =>[:create] do
        end
      end
    end
  end
  
  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
      resources :books, :only => [:show] do
        resources :sayfa_dys, :only =>[:show] do
        end
        resources :sayfa_ess, :only =>[:show] do
        end
        resources :sayfa_secs, :only =>[:show] do
        end
      end
   end
  end
  

  namespace :api, defaults: { format: :json } do
    resources :students, :only => [:show, :create] do
        resources :students_dyquestions, :only => [:show] do
        end
          resources :students_esquestions, :only => [:show] do
        end
          resources :students_secquestions, :only => [:show] do
        end
    end
  end

  
  root 'api#index'
  get 'index/teacher/:teachers_id'=>'api#index'
  get 'index/teacher/:teachers_id/aboutclique'=>'api#aboutClique'
  match '/login' => 'api#login', via: [:get  ]
  match '/login' => 'api#createssesion', via: [:post ]
  match '/login' => 'api#destroysession', via: [:delete ]
  
  match 'teacher/:teachers_id/wordsonbooks' => 'api#wordsbooks',via: [:get ]
  match 'teacher/:teachers_id/createwords/book/:books_id' => 'api#createWords',via: [:get ]
  match 'teacher/:teachers_id/createwords/book/:books_id' => 'api#createword',via: [:get, :post ]
  match 'teacher/:teachers_id/showwords/book/:books_id' => 'api#showWords',via: [:get ,:post ]
  match 'teacher/:teachers_id/showwords/book/:books_id' => 'api#destroyWord',via: [:delete ]
  match 'teacher/:teachers_id/updatewords/book/:books_id/word/:words_id' => 'api#updateWords',via: [:get , :patch ]
  match 'teacher/:teachers_id/updatewords/book/:books_id/word/:words_id' => 'api#updateword',via: [:patch, :get, :post]
  
  get 'teacher/:teachers_id/books/:books_id'=>'api#bookContent'
  get 'teacher/:teachers_id/books'=>'api#booksonsystem'
  get 'teacher/:id/classes' => 'api#subesList'
  get 'teacher/:teacher_id/classes/:subes_id' => 'api#classStudents'
  match 'teacher/:teachers_id/classesbooks/:subes_id' => 'api#showClassesBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/classesbooks/:subes_id' => 'api#destroyClassesBook', via: [:delete, :post, :get ]
  match 'teacher/:teachers_id/classesbooks/' => 'api#classesListBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/class/:subes_id/createclassesbooks/' => 'api#createClassesBooks' , via: [:get ,:post, :patch ]
  match 'teacher/:teachers_id/multiplechoicesbooks/' => 'api#showCoktanSecmeliBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/truefalsebooks/' => 'api#showDyBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/matchingbooks/' => 'api#showEslestirmeBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/createmultiplechoices/book/:books_id' => 'api#createCoktanSecmeliQuestions',via: [:get ]
  match 'teacher/:teachers_id/createmultiplechoices/book/:books_id' => 'api#createCoktanSecmeliQ',via: [:get, :post ]
  match 'teacher/:teachers_id/showmultiplechoices/book/:books_id' => 'api#showCoktanSecmeliQuestions',via: [:get ,:post ]
  match 'teacher/:teachers_id/showmultiplechoices/book/:books_id' => 'api#destroyCoktanSecmeliQ',via: [:delete ]
  match 'teacher/:teachers_id/updatemultiplechoices/book/:books_id/quesiton/:question_id' => 'api#updateCoktanSecmeliQuestion',via: [:get , :patch ]
  match 'teacher/:teachers_id/updatemultiplechoices/book/:books_id/quesiton/:question_id' => 'api#updateCoktanSecmeliQ',via: [:patch, :get, :post]
  
  match 'teacher/:teachers_id/createtfquestion/book/:books_id' => 'api#createDyQuestions',via: [:get ]
  match 'teacher/:teachers_id/createtfquestion/book/:books_id' => 'api#createDyQ',via: [:get, :post ]
  match 'teacher/:teachers_id/showtfquestion/book/:books_id' => 'api#showDyQuestions',via: [:get ,:post ]
  match 'teacher/:teachers_id/showtfquestion/book/:books_id' => 'api#destroydyQ',via: [:delete ]
  match 'teacher/:teachers_id/updatetfquestion/book/:books_id/question/:question_id' => 'api#updateDyQuestions',via: [:get , :patch ]
  match 'teacher/:teachers_id/updatetfquestion/book/:books_id/question/:question_id' => 'api#updateDyQ',via: [:patch, :get, :post]
  
  match 'teacher/:teachers_id/istatisticclasses' => 'api#istatistikSubeList',via: [:patch, :get, :post]
  match 'teacher/:teachers_id/istatisticclasses/:subes_id' => 'api#istatistikTotalOgrenci',via: [:patch, :get, :post]
  match '/teacher/:teachers_id/istatisticclass/:subes_id/student/:students_id' => 'api#istatistikOgrenci',via: [ :get]
  match '/teacher/:teachers_id/istatisticclass/:subes_id/student/:students_id/book/:books_id' => 'api#istatistikOgrenciBook',via: [ :get]
  
  match 'teacher/:teachers_id/showmatchquestion/book/:books_id' => 'api#showEslestirmeQuestions',via: [:get ,:post ]
  match 'teacher/:teachers_id/showmatchquestion/book/:books_id' => 'api#destroyEslestirmeQ',via: [:delete ]
  match 'teacher/:teachers_id/creatematchquestion/book/:books_id' => 'api#createEslestirmeQuestions',via: [:get ]
  match 'teacher/:teachers_id/creatematchquestion/book/:books_id' => 'api#createEslestirmeQ',via: [:get, :post ]
  match 'teacher/:teachers_id/updatematchquestion/book/:books_id/question/:question_id' => 'api#updateEslestirmeQuestions',via: [:get , :patch ]
  match 'teacher/:teachers_id/updatematchquestion/book/:books_id/question/:question_id' => 'api#updateEslestirmeQ',via: [:patch, :get, :post]

  match 'teacher/:teachers_id/createvideonotes/book/:books_id' => 'api#createVideoNotes', via: [:get  ]
  match 'teacher/:teachers_id/uploadvideonotes/book/:books_id' => 'api#upload', via: [:post ]
  match 'teacher/:teachers_id/teachervideonotes/' => 'api#showVideoNotesBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/showteachervideonotes/book/:books_id' => 'api#showVideoNotesBooks',via: [:get ,:post ]
  match 'teacher/:teachers_id/showteachervideonotes/' => 'api#showVideoNotesBooks', via: [:get ,:post ]
  
  match 'teacher/:teachers_id/createaudionotes/book/:books_id' => 'api#createAudioNotes', via: [:get  ]
  match 'teacher/:teachers_id/uploadaudionotes/book/:books_id' => 'api#uploadAudio', via: [:post ]
  match 'teacher/:teachers_id/teacheraudionotes/' => 'api#showAudioNotesBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/showteacheraudionotes/book/:books_id' => 'api#showAudioNotesBooks',via: [:get ,:post ]
  match 'teacher/:teachers_id/showteacheraudionotes/' => 'api#showAudioNotesBooks', via: [:get ,:post ]
  
  match 'teacher/:teachers_id/createtextnotes/book/:books_id' => 'api#createTextNotes', via: [:get  ]
   match 'teacher/:teachers_id/updatetextnotes/book/:books_id/note/:note_id' => 'api#updateTextNotes', via: [:get  ]
   match 'teacher/:teachers_id/updatetextnotes/book/:books_id/note/:note_id' => 'api#updateTextN', via: [:post  ] 
   match 'teacher/:teachers_id/showteachertextnotes/book/:books_id' => 'api#destroyTextNote',via: [:delete ]
  match 'teacher/:teachers_id/uploadtextnotes/book/:books_id' => 'api#uploadText', via: [:post ]
  match 'teacher/:teachers_id/teachertextnotes/' => 'api#showTextNotesBooks', via: [:get ,:post ]
  match 'teacher/:teachers_id/showteachertextnotes/book/:books_id' => 'api#showTextNotes',via: [:get ,:post ]
  match 'teacher/:teachers_id/showteachertextnotes/' => 'api#showTextNotesBooks', via: [:get ,:post ]
  
  match 'teacher/:teachers_id/messages' => 'api#showMessages',via: [:get]
  match 'teacher/:teachers_id/messages/:conversation_id' => 'api#showMessagesDetail',via: [:get]
  match 'teacher/:teachers_id/messages/:conversation_id/createmessage'=> 'api#createMessage',via: [:get]
  match 'teacher/:teachers_id/messages/:conversation_id/createmessage'=> 'api#createMsg',via: [:post]
end 


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

 
 
  
