class AddStudentToStudentChosenWord < ActiveRecord::Migration
  def change
    add_reference :student_chosen_words, :student, index: true, foreign_key: true
  end
end
