class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :kullaniciAdi
      t.string :sifre
      t.string :ogrenciFoto
      t.string :extraText
      t.integer :extraInt

      t.timestamps null: false
    end
  end
end
