class CreateDogruYanlisQuestions < ActiveRecord::Migration
  def change
    create_table :dogru_yanlis_questions do |t|
      t.string :soruText
      t.string :dogruCevap

      t.timestamps null: false
    end
  end
end
