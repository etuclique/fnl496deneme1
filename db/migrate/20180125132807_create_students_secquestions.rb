class CreateStudentsSecquestions < ActiveRecord::Migration
  def change
    create_table :students_secquestions do |t|
      t.belongs_to :student, index: true
      t.belongs_to :secmeli_question, index: true
      t.string :cevap
      t.timestamps
    end
  end
end
