class CreateJoinTable < ActiveRecord::Migration
  def change
  
    create_table :books_subes, id: false do |t|
      t.belongs_to :book, index: true
      t.belongs_to :sube, index: true
    end
  end
end
