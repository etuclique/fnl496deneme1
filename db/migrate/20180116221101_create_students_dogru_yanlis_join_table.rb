class CreateStudentsDogruYanlisJoinTable < ActiveRecord::Migration
  def change
    create_join_table :students, :dogru_yanlis_questions do |t|
      # t.index [:student_id, :dogru_yanlis_question_id]
      # t.index [:dogru_yanlis_question_id, :student_id]
    end
  end
end
