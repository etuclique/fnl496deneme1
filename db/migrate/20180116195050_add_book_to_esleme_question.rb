class AddBookToEslemeQuestion < ActiveRecord::Migration
  def change
    add_reference :esleme_questions, :book, index: true, foreign_key: true
  end
end
