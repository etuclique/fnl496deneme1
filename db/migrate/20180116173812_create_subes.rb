class CreateSubes < ActiveRecord::Migration
  def change
    create_table :subes do |t|
      t.integer :subeLevel
      t.string :subeHarf
      t.string :extraText
      t.integer :extraInt

      t.timestamps null: false
    end
  end
end
