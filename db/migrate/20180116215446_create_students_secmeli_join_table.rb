class CreateStudentsSecmeliJoinTable < ActiveRecord::Migration
  def change
    create_join_table :students, :secmeli_questions do |t|
      # t.index [:student_id, :secmeli_question_id]
      # t.index [:secmeli_question_id, :student_id]
    end
  end
end
