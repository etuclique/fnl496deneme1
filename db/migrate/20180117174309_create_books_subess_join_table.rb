class CreateBooksSubessJoinTable < ActiveRecord::Migration
  def change
    create_join_table :books, :subes do |t|
      # t.index [:book_id, :sube_id]
      t.index [:sube_id, :book_id], unique: true
    end
  end
end
