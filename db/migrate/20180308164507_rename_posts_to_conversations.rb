class RenamePostsToConversations < ActiveRecord::Migration
  def change
    rename_table :posts, :conversations
  end
end
