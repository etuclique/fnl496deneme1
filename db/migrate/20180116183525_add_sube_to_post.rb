class AddSubeToPost < ActiveRecord::Migration
  def change
    add_reference :posts, :sube, index: true, foreign_key: true
  end
end
