class AddSubeToStudent < ActiveRecord::Migration
  def change
    add_reference :students, :sube, index: true, foreign_key: true
  end
end
