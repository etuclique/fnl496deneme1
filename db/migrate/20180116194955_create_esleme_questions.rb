class CreateEslemeQuestions < ActiveRecord::Migration
  def change
    create_table :esleme_questions do |t|
      t.string :soruText
      t.string :dogruCevap

      t.timestamps null: false
    end
  end
end
