class CreateSubesBooksTable < ActiveRecord::Migration
  def change
    create_table :subes_books do |t|
      t.belongs_to :sube, index: true
      t.belongs_to :book, index: true
      t.timestamps
    end
  end
end
