class AddBookToSecmeliQuestion < ActiveRecord::Migration
  def change
    add_reference :secmeli_questions, :book, index: true, foreign_key: true
  end
end
