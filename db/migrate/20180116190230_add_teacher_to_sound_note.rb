class AddTeacherToSoundNote < ActiveRecord::Migration
  def change
    add_reference :sound_notes, :teacher, index: true, foreign_key: true
  end
end
