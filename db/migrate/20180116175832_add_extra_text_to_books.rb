class AddExtraTextToBooks < ActiveRecord::Migration
  def change
    add_column :books, :extraText, :string
    add_column :books, :extraInt, :integer
  end
end
