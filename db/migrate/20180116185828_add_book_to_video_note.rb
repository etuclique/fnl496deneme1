class AddBookToVideoNote < ActiveRecord::Migration
  def change
    add_reference :video_notes, :book, index: true, foreign_key: true
  end
end
