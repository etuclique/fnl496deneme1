class AddBookToStudentChosenWords < ActiveRecord::Migration
  def change
    add_reference :student_chosen_words, :book, index: true, foreign_key: true
  end
end
