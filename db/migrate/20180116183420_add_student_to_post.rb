class AddStudentToPost < ActiveRecord::Migration
  def change
    add_reference :posts, :student, index: true, foreign_key: true
  end
end
