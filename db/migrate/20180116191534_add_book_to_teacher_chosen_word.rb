class AddBookToTeacherChosenWord < ActiveRecord::Migration
  def change
    add_reference :teacher_chosen_words, :book, index: true, foreign_key: true
  end
end
