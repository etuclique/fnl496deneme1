class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :kitapAdi
      t.string :kitapYazari
      t.integer :kitapSayfaNo
      t.string :kitapHtmlIcerik
      t.text :kitapHtmlText
      t.string :kitapKapak

      t.timestamps null: false
    end
  end
end
