class CreateTeacherChosenWords < ActiveRecord::Migration
  def change
    create_table :teacher_chosen_words do |t|
      t.string :kelimetext

      t.timestamps null: false
    end
  end
end
