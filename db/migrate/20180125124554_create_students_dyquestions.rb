class CreateStudentsDyquestions < ActiveRecord::Migration
  def change
    create_table :students_dyquestions do |t|
      t.belongs_to :student, index: true
      t.belongs_to :dogru_yanlis_question, index: true
      t.string :cevap
      t.timestamps
    end
  end
end
