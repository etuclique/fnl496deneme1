class CreateBooksStudents < ActiveRecord::Migration
  def change
     create_table :students_books do |t|
      t.belongs_to :book, index: true
      t.belongs_to :student, index: true
      t.string :istatistik1
      t.timestamps
    end
  end
end
