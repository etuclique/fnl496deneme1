class CreateSecmeliQuestions < ActiveRecord::Migration
  def change
    create_table :secmeli_questions do |t|
      t.string :soruText
      t.string :aText
      t.string :bText
      t.string :cText
      t.string :dText
      t.string :dogruCevap

      t.timestamps null: false
    end
  end
end
