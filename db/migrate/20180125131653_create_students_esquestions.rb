class CreateStudentsEsquestions < ActiveRecord::Migration
  def change
    create_table :students_esquestions do |t|
      t.belongs_to :student, index: true
      t.belongs_to :esleme_question, index: true
      t.string :cevap
      t.timestamps
    end
  end
end
