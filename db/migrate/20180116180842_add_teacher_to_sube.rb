class AddTeacherToSube < ActiveRecord::Migration
  def change
    add_reference :subes, :teacher, index: true, foreign_key: true
  end
end
