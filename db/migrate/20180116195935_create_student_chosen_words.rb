class CreateStudentChosenWords < ActiveRecord::Migration
  def change
    create_table :student_chosen_words do |t|
      t.string :kelimeText

      t.timestamps null: false
    end
  end
end
