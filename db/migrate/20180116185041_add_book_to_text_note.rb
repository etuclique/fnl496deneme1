class AddBookToTextNote < ActiveRecord::Migration
  def change
    add_reference :text_notes, :book, index: true, foreign_key: true
  end
end
