class CreateTextNotes < ActiveRecord::Migration
  def change
    create_table :text_notes do |t|
      t.string :yaziliNotIcerik

      t.timestamps null: false
    end
  end
end
