class RenameEslemeQuestionDogruCevapToDogruHarf < ActiveRecord::Migration
  def change
    rename_column :esleme_questions, :dogruCevap, :dogruHarf
  end
end
