class AddTeacherToTextNote < ActiveRecord::Migration
  def change
    add_reference :text_notes, :teacher, index: true, foreign_key: true
  end
end
