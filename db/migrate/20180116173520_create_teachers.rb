class CreateTeachers < ActiveRecord::Migration
  def change
    create_table :teachers do |t|
      t.string :kullaniciAdi
      t.string :sifre
      t.string :email
      t.string :extraText

      t.timestamps null: false
    end
  end
end
