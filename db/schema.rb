# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180315195222) do

  create_table "books", force: :cascade do |t|
    t.string   "kitapAdi"
    t.string   "kitapYazari"
    t.integer  "kitapSayfaNo"
    t.string   "kitapHtmlIcerik"
    t.text     "kitapHtmlText"
    t.string   "kitapKapak"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "extraText"
    t.integer  "extraInt"
  end

  create_table "conversations", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "teacher_id"
    t.integer  "student_id"
    t.integer  "sube_id"
    t.string   "konu"
  end

  add_index "conversations", ["student_id"], name: "index_conversations_on_student_id"
  add_index "conversations", ["sube_id"], name: "index_conversations_on_sube_id"
  add_index "conversations", ["teacher_id"], name: "index_conversations_on_teacher_id"

  create_table "dogru_yanlis_questions", force: :cascade do |t|
    t.string   "soruText"
    t.string   "dogruCevap"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "book_id"
    t.integer  "sayfaNo"
  end

  add_index "dogru_yanlis_questions", ["book_id"], name: "index_dogru_yanlis_questions_on_book_id"

  create_table "esleme_questions", force: :cascade do |t|
    t.string   "soruText"
    t.string   "dogruHarf"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "book_id"
    t.string   "yandakiText"
    t.integer  "sayfaNo"
    t.integer  "ortak_id"
  end

  add_index "esleme_questions", ["book_id"], name: "index_esleme_questions_on_book_id"
  add_index "esleme_questions", ["ortak_id"], name: "index_esleme_questions_on_ortak_id"

  create_table "messages", force: :cascade do |t|
    t.string   "body"
    t.boolean  "sender"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "conversation_id"
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id"

  create_table "secmeli_questions", force: :cascade do |t|
    t.string   "soruText"
    t.string   "aText"
    t.string   "bText"
    t.string   "cText"
    t.string   "dText"
    t.string   "dogruCevap"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "book_id"
    t.integer  "sayfaNo"
  end

  add_index "secmeli_questions", ["book_id"], name: "index_secmeli_questions_on_book_id"

  create_table "sound_notes", force: :cascade do |t|
    t.string   "sesliNotIcerik"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "teacher_id"
    t.integer  "book_id"
    t.integer  "sayfaNo"
  end

  add_index "sound_notes", ["book_id"], name: "index_sound_notes_on_book_id"
  add_index "sound_notes", ["teacher_id"], name: "index_sound_notes_on_teacher_id"

  create_table "student_chosen_words", force: :cascade do |t|
    t.string   "kelimeText"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "student_id"
    t.integer  "book_id"
  end

  add_index "student_chosen_words", ["book_id"], name: "index_student_chosen_words_on_book_id"
  add_index "student_chosen_words", ["student_id"], name: "index_student_chosen_words_on_student_id"

  create_table "students", force: :cascade do |t|
    t.string   "kullaniciAdi"
    t.string   "sifre"
    t.string   "ogrenciFoto"
    t.string   "extraText"
    t.integer  "extraInt"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "sube_id"
    t.string   "password_digest"
    t.string   "email"
  end

  add_index "students", ["sube_id"], name: "index_students_on_sube_id"

  create_table "students_books", force: :cascade do |t|
    t.integer  "book_id"
    t.integer  "student_id"
    t.string   "istatistik1"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "students_books", ["book_id"], name: "index_students_books_on_book_id"
  add_index "students_books", ["student_id"], name: "index_students_books_on_student_id"

  create_table "students_dyquestions", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "dogru_yanlis_question_id"
    t.string   "cevap"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "students_dyquestions", ["dogru_yanlis_question_id"], name: "index_students_dyquestions_on_dogru_yanlis_question_id"
  add_index "students_dyquestions", ["student_id"], name: "index_students_dyquestions_on_student_id"

  create_table "students_esquestions", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "esleme_question_id"
    t.string   "cevap"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "students_esquestions", ["esleme_question_id"], name: "index_students_esquestions_on_esleme_question_id"
  add_index "students_esquestions", ["student_id"], name: "index_students_esquestions_on_student_id"

  create_table "students_secquestions", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "secmeli_question_id"
    t.string   "cevap"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "students_secquestions", ["secmeli_question_id"], name: "index_students_secquestions_on_secmeli_question_id"
  add_index "students_secquestions", ["student_id"], name: "index_students_secquestions_on_student_id"

  create_table "subes", force: :cascade do |t|
    t.integer  "subeLevel"
    t.string   "subeHarf"
    t.string   "extraText"
    t.integer  "extraInt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "teacher_id"
  end

  add_index "subes", ["teacher_id"], name: "index_subes_on_teacher_id"

  create_table "subes_books", force: :cascade do |t|
    t.integer  "sube_id"
    t.integer  "book_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subes_books", ["book_id"], name: "index_subes_books_on_book_id"
  add_index "subes_books", ["sube_id"], name: "index_subes_books_on_sube_id"

  create_table "teacher_chosen_words", force: :cascade do |t|
    t.string   "kelimetext"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "teacher_id"
    t.integer  "book_id"
  end

  add_index "teacher_chosen_words", ["book_id"], name: "index_teacher_chosen_words_on_book_id"
  add_index "teacher_chosen_words", ["teacher_id"], name: "index_teacher_chosen_words_on_teacher_id"

  create_table "teachers", force: :cascade do |t|
    t.string   "kullaniciAdi"
    t.string   "sifre"
    t.string   "email"
    t.string   "extraText"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
  end

  create_table "text_notes", force: :cascade do |t|
    t.string   "yaziliNotIcerik"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "book_id"
    t.integer  "teacher_id"
    t.integer  "sayfaNo"
  end

  add_index "text_notes", ["book_id"], name: "index_text_notes_on_book_id"
  add_index "text_notes", ["teacher_id"], name: "index_text_notes_on_teacher_id"

  create_table "video_notes", force: :cascade do |t|
    t.string   "videoNotIcerik"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "teacher_id"
    t.integer  "book_id"
    t.integer  "sayfaNo"
  end

  add_index "video_notes", ["book_id"], name: "index_video_notes_on_book_id"
  add_index "video_notes", ["teacher_id"], name: "index_video_notes_on_teacher_id"

end
