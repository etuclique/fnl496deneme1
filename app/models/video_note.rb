class VideoNote < ActiveRecord::Base
    belongs_to:teacher
    belongs_to:book
    
    def self.ogrencininKitaptakiVideoNotlari(teacher,book)
        return teacher.video_notes.where(book_id:book)
    end
    
end
