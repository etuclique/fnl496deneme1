class Student < ActiveRecord::Base
    belongs_to:sube
    has_many:conversations
    has_many:student_chosen_words
    has_many :StudentsBook 
    has_many :books, :through => :StudentsBook
    has_many:StudentsDyquestion
    has_many :dogru_yanlis_questions, :through => :StudentsDyquestion
    has_many:StudentsEsquestion
    has_many :esleme_questions, :through => :StudentsEsquestion
    has_many:StudentsSecquestion
    has_many :secmeli_questions, :through => :StudentsSecquestion
    has_secure_password
end
