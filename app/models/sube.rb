class Sube < ActiveRecord::Base
    has_many:students
    has_many:conversations
    belongs_to:teacher
    has_many :SubesBook 
    has_many :books, :through => :SubesBook
end
