class StudentChosenWord < ActiveRecord::Base
    
    belongs_to:student
    belongs_to:book
    
    def self.ogrencininKitaptakiKelimeleri(student,book)
        return student.student_chosen_words.where(book_id:book)
    end
end
