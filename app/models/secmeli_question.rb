class SecmeliQuestion < ActiveRecord::Base
    belongs_to:book
     has_many:StudentsSecquestion
    has_many :students, :through => :StudentsSecquestion
    
    def self.ogrencininKitaptakiSayfaBazliSorulari(book)
        return secmeli_questions.where(book_id:book)
    end
    
    def self.kitaptakiSecSoruSizeAl(book, student)
        return student.secmeli_questions.where(book_id:book).count();
    end
end
