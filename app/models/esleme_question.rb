class EslemeQuestion < ActiveRecord::Base
    belongs_to:book
    has_many:StudentsEsquestion
    has_many :students, :through => :StudentsEsquestion
    
    def self.ogrencininKitaptakiSayfaBazliSorulari(book)
        return esleme_questions.where(book_id:book)
    end
    
    def self.kitaptakiEsSoruSizeAl(book, student)
        return student.esleme_questions.where(book_id:book).count();
    end
end
