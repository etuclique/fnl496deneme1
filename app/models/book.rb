class Book < ActiveRecord::Base

   has_many:text_notes
   has_many:video_notes
   has_many:sound_notes
   has_many:teacher_chosen_words
   has_many:secmeli_questions
   has_many:esleme_questions
   has_many:dogru_yanlis_questions
   has_many:student_chosen_words
   has_many :StudentsBook 
   has_many :students, :through => :StudentsBook
   has_many :SubesBook 
   has_many :subes, :through => :SubesBook
end
