class DogruYanlisQuestion < ActiveRecord::Base
    belongs_to:book
    has_many:StudentsDyquestion
    has_many :students, :through => :StudentsDyquestion

    def self.ogrencininKitaptakiSayfaBazliSorulari(book)
        return dogru_yanlis_questions.where(book_id:book)
    end
    
    def self.kitaptakiDySoruSizeAl(book, student)
        return student.dogru_yanlis_questions.where(book_id:book).count();
    end
    
end
