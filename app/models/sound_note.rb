class SoundNote < ActiveRecord::Base
    belongs_to:teacher
    belongs_to:book
    
    def self.ogrencininKitaptakiSesNotlari(teacher,book)
        return teacher.sound_notes.where(book_id:book)
    end

end
