class TextNote < ActiveRecord::Base
    belongs_to:teacher
    belongs_to:book
    
    def self.ogrencininKitaptakiTextNotlari(teacher,book)
        return teacher.text_notes.where(book_id:book)
    end
end
