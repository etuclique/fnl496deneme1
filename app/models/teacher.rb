class Teacher < ActiveRecord::Base
    has_many:subes
    has_many:conversations
    has_many:text_notes
    has_many:video_notes
    has_many:sound_notes
    has_many:teacher_chosen_words
    has_secure_password
end