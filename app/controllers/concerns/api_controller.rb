class ApiController < ApplicationController
      respond_to :html, :json, :js 
     skip_before_filter  :verify_authenticity_token
     
    def new
        @books = Book.all
        
    end

    def index
         @teacher = Teacher.find_by_id(session[:current_teacher_id])
    end
    def aboutClique
         @teacher = Teacher.find_by_id(session[:current_teacher_id])

    end
    def login
    end

    
    def createssesion
        user = Teacher.find_by(email: params[:session][:email].downcase)
         if user && user.authenticate(params[:session][:password])
            session[:current_teacher_id]=user.id
            redirect_to(:action=>'index')
        else
            flash.now[:danger] = 'Invalid email/password combination'
            render 'login'
        end
    end
    def destroysession
        session[:current_teacher_id] = nil
        flash[:notice] = "You have successfully logged out."
        redirect_to(:action=>'login')
    end
    
    def wordsbooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @booksall = Book.all
    end
    def showWords
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @wordsbooksall = Book.find(params[:books_id])
        @wordall = @wordsbooksall.teacher_chosen_words.all
    end
    def createWords
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
    end
    def createword
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
                 
        @mcq=@mcbook.teacher_chosen_words.new(:kelimetext => params[:kelimetext])
        @mcq.book_id = params[:books_id]
        @mcq.teacher_id= @teacher.id
        if @mcq.valid?
             @mcq.save
             redirect_to(:action=>'showWords')
            
        end

    end
    def destroyWord
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @book = Book.find(params[:books_id])
        @deleteWord = @book.teacher_chosen_words.find(params[:id])
        if @deleteWord.destroy!
            redirect_to(:action=>'showWords')
            
        end
    end
    def updateWords
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
        @mcq = @mcbook.teacher_chosen_words.find(params[:words_id])     

    end
    def updateword
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
        @mcq = @mcbook.teacher_chosen_words.find(params[:words_id])     
        
        if @mcq.update(word_update_params)
             
             redirect_to(:action=>'showWords')
            
        end

    end
    def word_update_params
    #params.require(:issue).permit(:title, :content, :dueDate)
         params.permit(:kelimetext)
    end
    
    def booksonsystem
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        respond_with(@bookAll =Book.all)
    end
    def bookContent
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        respond_with(@book =Book.find(params[:books_id]))
       
    end
    def subesList
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        respond_with(@subeAll = @teacher.subes.all)
    end
    def classStudents
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @sube = Sube.find(params[:subes_id])
        respond_with(@students = @sube.students.all)
    end
    def classesListBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        respond_with(@subeAll = @teacher.subes.all)
    end
    def showClassesBooks
     
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @sube = Sube.find(params[:subes_id])
        @classesbooksall = @sube.books
    end
    def destroyClassesBook
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @sube = Sube.find(params[:subes_id])
        @deletebook = @sube.books.find(params[:id])
        @al = SubesBook.find_by(sube_id: params[:subes_id], book_id: params[:id])
        
        if @al.destroy!
            redirect_to(:action=>'showClassesBooks')
            
        end
        
        
    end

    def createClassesBooks
        @teacher = Teacher.find(params[:teachers_id])
        @books = Book.all
        @sube = Sube.find(params[:subes_id])
        @subesBookAll = @sube.books.all
        
        puts params[:books_id].inspect
            if !params[:books_id].nil? && !params[:books_id].empty?
                  Array(params[:books_id]).each do |f| 
                    if !SubesBook.exists?(:sube_id => @sube.id , :book_id => f)
                        @subeBook  = SubesBook.new(:sube_id => @sube.id , :book_id => f)
                         if @subeBook.valid?
                            @subeBook.save
                           
                         end
                    
                    else
                    end
                    
                end
                redirect_to(:action=>'showClassesBooks')
            end
             
        
     
     
    end
       
    def showCoktanSecmeliBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @multiplechoicesbooksall = Book.all
    end
    def showCoktanSecmeliQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @multiplechoicesbooksall = Book.find(params[:books_id])
        @mcqall = @multiplechoicesbooksall.secmeli_questions.all
    end
    
    def createCoktanSecmeliQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])


    end
    def createCoktanSecmeliQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
                 
        @mcq=@mcbook.secmeli_questions.new(:soruText => params[:soruText], :aText => params[:aText], :bText => params[:bText],:cText => params[:cText], :dText => params[:dText] ,:dogruCevap => params[:dogruCevap],:sayfaNo => params[:sayfaNo])
        @mcq.book_id = params[:books_id]
        
        if @mcq.valid?
             @mcq.save
             redirect_to(:action=>'showCoktanSecmeliQuestions')
            
        end

    end
    def destroyCoktanSecmeliQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @book = Book.find(params[:books_id])
        @deleteCoktanSecmeli = @book.secmeli_questions.find(params[:id])
        if @deleteCoktanSecmeli.destroy!
            redirect_to(:action=>'showCoktanSecmeliQuestions')
            
        end
    end
    def updateCoktanSecmeliQuestion
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
        @mcq = @mcbook.secmeli_questions.find(params[:question_id])     

    end
    def updateCoktanSecmeliQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mcbook = Book.find(params[:books_id])
        @mcq = @mcbook.secmeli_questions.find(params[:question_id])     
        
        if @mcq.update(mcq_update_params)
             
             redirect_to(:action=>'showCoktanSecmeliQuestions')
            
        end

    end
    def showDyBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbookall = Book.all
    end
    def showDyQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbookall = Book.find(params[:books_id])
        @tfquestionall=@tfbookall.dogru_yanlis_questions.all
        
    end
    def createDyQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbook = Book.find(params[:books_id])
       
    end
    def createDyQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbook = Book.find(params[:books_id])
                 
        @dyq=@tfbook.dogru_yanlis_questions.new(:soruText => params[:soruText],:dogruCevap => params[:dogruCevap],:sayfaNo => params[:sayfaNo])
        @dyq.book_id = params[:books_id]
        
        if @dyq.valid?
             @dyq.save
             redirect_to(:action=>'showDyQuestions')
            
        end

    end
    def destroydyQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @book = Book.find(params[:books_id])

        @deleteDy = @book.dogru_yanlis_questions.find(params[:id])
        if @deleteDy.destroy!

            redirect_to(:action=>'showDyQuestions')
            
        end
    end
    def updateDyQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbook = Book.find(params[:books_id])
        @dyq = @tfbook.dogru_yanlis_questions.find(params[:question_id])     

    end
    def updateDyQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbook = Book.find(params[:books_id])
        @dyq = @tfbook.dogru_yanlis_questions.find(params[:question_id])     
        
        if @dyq.update(dyq_update_params)
             
             redirect_to(:action=>'showDyQuestions')
            
        end

    end
    
    def showEslestirmeBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @matchbook = Book.all
    end
    
    def showEslestirmeQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @matchbook = Book.find(params[:books_id])
        @matchquestionall=@matchbook.esleme_questions.all
    end   
    
    def createEslestirmeQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
    end
    def createEslestirmeQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
                 
        @matchquestionall=@mbook.esleme_questions.new(:soruText => params[:soruText], :dogruHarf => params[:dogruHarf], :yandakiText => params[:yandakiText],:sayfaNo => params[:sayfaNo])
        @matchquestionall.book_id = params[:books_id]
        
        if @matchquestionall.valid?
             @matchquestionall.save
             redirect_to(:action=>'showEslestirmeQuestions')
            
        end

    end
    def destroyEslestirmeQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @book = Book.find(params[:books_id])
        @deleteEslestirme = @book.esleme_questions.find(params[:id])
        if @deleteEslestirme.destroy!
            redirect_to(:action=>'showEslestirmeQuestions')
            
        end
    end
    
    def updateEslestirmeQuestions
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
        @matchquestion = @mbook.esleme_questions.find(params[:question_id])     

    end
    def updateEslestirmeQ
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
        @matchquestion = @mbook.esleme_questions.find(params[:question_id])     
        
        if @matchquestion.update(mtq_update_params)
             redirect_to(:action=>'showEslestirmeQuestions')
        end   
    end
    def istatistikSubeList
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        respond_with(@subeAll = @teacher.subes.all)
    end
    def istatistikTotalOgrenci
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @sube = Sube.find(params[:subes_id])
        respond_with(@students = @sube.students.all)
    end
    def istatistikOgrenci
        
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
          
        @sube = Sube.find(params[:subes_id])
        
        @student = Student.find(params[:students_id])
        @classesbooksall = @sube.books
      
        
    end
    def istatistikOgrenciBook
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
          
        @sube = Sube.find(params[:subes_id])
        
        @student = Student.find(params[:students_id])
        @book =Book.find(params[:books_id])
        @studentChosenWords = StudentChosenWord.where(student_id: params[:students_id],book_id: params[:books_id])
        @studentChosenWordsCount = StudentChosenWord.where(student_id: params[:students_id],book_id: params[:books_id]).count
       
        @bookPageNumber = @book.kitapSayfaNo.to_d
        @bookStudent = StudentsBook.where(student_id: params[:students_id],book_id: params[:books_id])
        if !@bookStudent.first.nil?
                @bookStudentPageNumber = @bookStudent.first.istatistik1.to_d
                @bookPercentage = (@bookStudentPageNumber/@bookPageNumber)*100
        else
                @bookStudentPageNumber = 0
                @bookPercentage = 0*100
        end
        @coktanSecmeliStAnswers = []
        @coktanSecmeliQuAnswers = []
        @coktanSecmeliQuFlags = []
 
        @coktanSecmeliQuestion = SecmeliQuestion.where(book_id: params[:books_id])
        
        @coktanSecmeliQuestion.each do |category|
             @tposts = StudentsSecquestion.where(student_id: params[:students_id],secmeli_question_id: category.id)
             if !@tposts.first.nil?
                 @coktanSecmeliQu = SecmeliQuestion.where(id: category.id)
                 @coktanSecmeliQuAnswers += @coktanSecmeliQu
                 @coktanSecmeliStAnswers += @tposts 
                 
                     if  @coktanSecmeliQu.first.dogruCevap == @tposts.first.cevap
                         @coktanSecmeliQuFlags << true
                     else
                        @coktanSecmeliQuFlags << false
                     end
             else

             end
             
        end
        @coktanSecmeliQuestionCount = @coktanSecmeliQuestion.count
        @coktanSecmeliStAnswersCount = @coktanSecmeliStAnswers.count
        @percentageCoktanSecmeli= (@coktanSecmeliStAnswersCount.to_d/@coktanSecmeliQuestionCount.to_d)*100
        
        @tfStAnswers = []
        @tfQuAnswers = []
        @tfQuFlags = []
 
        @tfQuestion = DogruYanlisQuestion.where(book_id: params[:books_id])
        
        @tfQuestion.each do |category|
             @tfposts = StudentsDyquestion.where(student_id: params[:students_id],dogru_yanlis_question_id: category.id)
             if !@tfposts.first.nil?
                 @tfQu = DogruYanlisQuestion.where(id: category.id)
                 @tfQuAnswers += @tfQu
                 @tfStAnswers += @tfposts 
                 
                     if  @tfQu.first.dogruCevap == @tfposts.first.cevap
                         @tfQuFlags << true
                     else
                        @tfQuFlags << false
                     end
             else

             end
             
        end
        @tfQuestionCount = @tfQuestion.count
        @tfStAnswersCount = @tfStAnswers.count
        @percentageTf = (@tfStAnswersCount.to_d/@tfQuestionCount.to_d)*100
      
      
    end
 
    def mtq_update_params
    #params.require(:issue).permit(:title, :content, :dueDate)

        

        params.permit(:soruText, :dogruHarf, :yandakiText , :sayfaNo)

        
    end    
    def dyq_update_params
    #params.require(:issue).permit(:title, :content, :dueDate)

        params.permit(:soruText,:dogruCevap,:sayfaNo)


        
    end    
    def note_update_params
    #params.require(:issue).permit(:title, :content, :dueDate)
         params.permit(:yaziliNotIcerik,:sayfaNo)
    end
    def mcq_update_params
    #params.require(:issue).permit(:title, :content, :dueDate)
         params.permit(:soruText, :aText, :bText, :cText , :dText, :dogruCevap, :sayfaNo)
    end
    def upload
     require 'streamio-ffmpeg'   
     uploaded_io = params[:videoNotIcerik]
     File.open(Rails.root.join('app','assets', 'stylesheets','videos', uploaded_io.original_filename), 'wb') do |file|
        file.write(uploaded_io.read)
    end
    @teacher = Teacher.find(params[:teachers_id])
    @mbook = Book.find(params[:books_id])
    @photo = VideoNote.new(:videoNotIcerik => uploaded_io.original_filename,:teacher_id => @teacher.id,:book_id=> @mbook.id,:sayfaNo=> params[:sayfaNo] ) 
    # normal save
    
    if @photo.save
        redirect_to(:action=>'createVideoNotes')
    end
    end
    
    def uploadAudio
        
     uploaded_io1 = params[:sesliNotIcerik]
     File.open(Rails.root.join('app','assets', 'stylesheets','sounds', uploaded_io1.original_filename), 'wb') do |file|
        file.write(uploaded_io1.read)
    end
    @teacher = Teacher.find_by_id(session[:current_teacher_id])
    @mbook = Book.find(params[:books_id])
    @sound = SoundNote.new(:sesliNotIcerik => uploaded_io1.original_filename,:teacher_id => @teacher.id,:book_id=> @mbook.id,:sayfaNo=> params[:sayfaNo] ) 
    
    # normal save
    
     if @sound.save
        redirect_to(:action=>'createAudioNotes')
     end
    end
    
    def uploadText

    @teacher = Teacher.find_by_id(session[:current_teacher_id])
    @mbook = Book.find(params[:books_id])
    @text = TextNote.new(:yaziliNotIcerik => params[:yaziliNotIcerik],:teacher_id => @teacher.id,:book_id=> @mbook.id,:sayfaNo=> params[:sayfaNo]) 
    
    # normal save
    if @text.save
        redirect_to(:action=>'showTextNotes')
    
    end
    end
    def showTextNotes
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbookall = Book.find(params[:books_id])
        @tfquestionall=@tfbookall.text_notes.all
    end
    def updateTextNotes
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbook = Book.find(params[:books_id])
        @dyq = @tfbook.text_notes.find(params[:note_id])  
    end
    def updateTextN
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @tfbook = Book.find(params[:books_id])
        @dyq = @tfbook.text_notes.find(params[:note_id])     
        
        if @dyq.update(note_update_params)
             
             redirect_to(:action=>'showTextNotes')
            
        end
    end
    def destroyTextNote
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @book = Book.find(params[:books_id])

        @deleteDy = @book.text_notes.find(params[:id])
        if @deleteDy.destroy!

            redirect_to(:action=>'showTextNotes')
            
        end
    end
    def showVideoNotesBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @Notesbooksall = Book.all
    end
    def showAudioNotesBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @Notesbooksall = Book.all
    end
    def showTextNotesBooks
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @Notesbooksall = Book.all
    end
    def createVideoNotes
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
        
    end
    def createAudioNotes
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
        
    end
    def createTextNotes
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @mbook = Book.find(params[:books_id])
        
    end
   
	def showMessages
	    @teacher = Teacher.find_by_id(session[:current_teacher_id])
	    @messages = @teacher.conversations.all

	end
	def showMessagesDetail
	    @teacher = Teacher.find_by_id(session[:current_teacher_id])
	    @converation_id = params[:conversation_id]
	    @messages = Message.where(conversation_id: params[:conversation_id])

	end
	def createMessage
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
        @converation_id = params[:conversation_id]
    end
    def createMsg
        @teacher = Teacher.find_by_id(session[:current_teacher_id])
                 
        @newmsg=Message.where(conversation_id: params[:conversation_id]).new(:body => params[:body], :sender => 'false')

        if @newmsg.valid?
             @newmsg.save
             redirect_to(:action=>'showMessagesDetail')
            
        end

    end

	def new
		@message = current_user.messages.build
	end

	def create
		@message = current_user.messages.build(message_params)
		if @message.save
			redirect_to root_path
		else
			render 'new'
		end
	end

	def update
		if @message.update(message_params)
			redirect_to message_path
		else
			render 'edit'
		end
	end

	def destroy
		@message.destroy
		redirect_to root_path
	end
end
