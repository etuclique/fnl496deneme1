class Api::SayfaEssController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
    def index
    book = Book.find(params[:book_id])
    respond_with EslemeQuestion.ogrencininKitaptakiSayfaBazliSorulari(book)
    end
  
  
  def show
   respond_with EslemeQuestion.where(book_id: params[:book_id],sayfaNo: params[:id])
  end
  
end
  
