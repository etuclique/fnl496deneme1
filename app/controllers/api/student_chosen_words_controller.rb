class Api::StudentChosenWordsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    respond_with StudentChosenWord.ogrencininKitaptakiKelimeleri(student,book)
  end
  
  def show
    StudentChosenWord.find(params[:id])
  end
 
  def create
    student = Student.find(params[:student_id])
    word=student.student_chosen_words.new(words_params)
    word.book_id = params[:book_id]
    if word.save
        render json: @issue, status: 201 
    else
        render json: { errors: @issue.errors}, status: 422 
    end
  end
  
    # Deleting users
  def destroy
    student = Student.find(params[:student_id])
    word = student.student_chosen_words.find(params[:id])
    word.destroy!
    
    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end
  
  
  def words_params
    params.permit(:id,:kelimeText)
  end
end
  
