class Api::BooksController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def kitapicerik
    book= Book.find(params[:id])
       render :json => book.to_json( :only => [:id,:kitapHtmlIcerik] )
    
  end

  def show
       book= Book.find(params[:id])
       render :json => book.to_json( :only => [:id,:kitapAdi,:kitapYazari,:kitapSayfaNo,:kitapHtmlText,:kitapKapak] )
  end
  
  def index
    respond_with Book.all
    
  end




  # # Creating users
  # def create
  #   book=Book.new(book_params)

  #   if book.save 
  #     render json: book, status: 201 
  #   else
  #     render json: { errors: book.errors}, status: 422
  #   end
  # end

  # # Updating users
  # def update
  #   book = Book.find(params[:id])

  #   if user.update(book_params)
  #     render json: book, status: 200
  #   else
  #     render json: { errors: book.errors }, status: 422
  #   end
  # end

  # # Deleting users
  # def destroy
  #   book = Book.find(params[:id])
  #   book.destroy
  #   head 204
  # end

  # private

  # def book_params
  # params.require(:book).permit(:email, :password, :password_confirmation)
  # end
end
  
