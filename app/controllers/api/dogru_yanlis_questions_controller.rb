class Api::DogruYanlisQuestionsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    respond_with DogruYanlisQuestion.all
    
  end
  
  def show
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    respond_with student.dogru_yanlis_questions.where(book_id: params[:books_id])
  end
  
  def kitaptakiDySoruSize
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    respond_with DogruYanlisQuestion.kitaptakiDySoruSizeAl(student,book)
  end
    
 

end



