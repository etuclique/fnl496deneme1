class Api::TeachersController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    respond_with Teacher.all
    
  end

  def show
       respond_with Teacher.find(params[:id])
  end
    
  
end