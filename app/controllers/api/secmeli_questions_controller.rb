class Api::SecmeliQuestionsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    respond_with SecmeliQuestion.all
    
  end
  
    def show
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    respond_with student.secmeli_questions.where(book_id: params[:books_id])
  end
  
  
  def kitaptakiSecSoruSize
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    respond_with SecmeliQuestion.kitaptakiSecSoruSizeAl(student,book)
  end
  
  def ogrencininCevapladigiSecSoruSize
    book = Book.find(params[:book_id])
    respond_with SecmeliQuestion.ogrencininCevapladigiSecSoruSizeAl(book)
  end

  

   # def show
   #      respond_with SecmeliQuestions.find(params[:id])
  #  end
    
  




  # # Creating users
  # def create
  #   book=Book.new(book_params)

  #   if book.save 
  #     render json: book, status: 201 
  #   else
  #     render json: { errors: book.errors}, status: 422
  #   end
  # end

  # # Updating users
  # def update
  #   book = Book.find(params[:id])

  #   if user.update(book_params)
  #     render json: book, status: 200
  #   else
  #     render json: { errors: book.errors }, status: 422
  #   end
  # end

  # # Deleting users
  # def destroy
  #   book = Book.find(params[:id])
  #   book.destroy
  #   head 204
  # end

  # private

  # def book_params
  # params.require(:book).permit(:email, :password, :password_confirmation)
  # end
end
  
