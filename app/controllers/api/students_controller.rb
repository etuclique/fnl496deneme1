class Api::StudentsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    respond_with Student.all
  end

  def show
       respond_with Student.find(params[:id])
  end
    
  def login
    user = Student.find_by(email: params[:email].downcase)
    if user && user.authenticate(params[:password])
        session[:current_student_id]=user.id
        render json: user, status: 201 
    else
      render json: {}, status: 422  
    end
  end
  
  def logout
    session[:current_student_id] = nil
  end
  
  def ogrencininogretmeni
    user = Student.find(params[:student_id])
    sube = Sube.find(user.sube_id)
    teacher = Teacher.find(sube.teacher_id)
    render :json => teacher.to_json( :only => [:id,:kullaniciAdi,:email] ) 
  end
  
  def ogrencininkitaplari
    student = Student.find(params[:id])
    sube = Sube.find(student.sube_id)
    book= Book.find(params[:id])
    render :json => sube.books.to_json( :only => [:id,:kitapAdi,:kitapYazari,:kitapSayfaNo,:kitapHtmlText,:kitapKapak] )
 
  end
  
  def ogrencininTestSonuclari
  
    dogruYanlisSorular = DogruYanlisQuestion.where(book_id: params[:book_id])
    secmeliSorular = SecmeliQuestion.where(book_id: params[:book_id])
  
    dogruYanlisDogruCevapSayisi=0
    secmeliDogruCevapSayisi=0
    
    dogruYanlisYanlisCevapSayisi=0
    secmeliYanlisCevapSayisi=0
    
   
    dogruYanlisToplamSoruSayisi=dogruYanlisSorular.count();
    secmeliToplamSoruSayisi=secmeliSorular.count();
    
  
    dogruYanlisToplamCozmusOlduguSoruSayisi=0;
    secmeliToplamCozmusOlduguSoruSayisi=0;
    
    
    dogruYanlisSorular.each do |category|
        studentsoru = StudentsDyquestion.where(student_id: params[:student_id],dogru_yanlis_question_id: category.id)
        if !studentsoru.first.nil?
            soru = DogruYanlisQuestion.where(id: category.id)
            dogruYanlisToplamCozmusOlduguSoruSayisi+=1

            if  soru.first.dogruCevap == studentsoru.first.cevap
                dogruYanlisDogruCevapSayisi+=1
            else
                dogruYanlisYanlisCevapSayisi+=1
            end
        else
    
        end
    
    end
    
    secmeliSorular.each do |category|
        studentsoru = StudentsSecquestion.where(student_id: params[:student_id],secmeli_question_id: category.id)
        if !studentsoru.first.nil?
            soru = SecmeliQuestion.where(id: category.id)
            secmeliToplamCozmusOlduguSoruSayisi+=1

            if  soru.first.dogruCevap == studentsoru.first.cevap
                secmeliDogruCevapSayisi+=1
            else
                secmeliYanlisCevapSayisi+=1
            end
        else
    
        end
    
    end
    
    
    
    render json: [{soru_type:"dogruYanlisQuestion",dogruCevapSayisi:dogruYanlisDogruCevapSayisi,yanlisCevapSayisi:dogruYanlisYanlisCevapSayisi,toplamCozulmusSoruSayisi:dogruYanlisToplamCozmusOlduguSoruSayisi,toplamSoruSayisi:dogruYanlisToplamSoruSayisi},{soru_type:"secmeliQuestion",dogruCevapSayisi:secmeliDogruCevapSayisi,yanlisCevapSayisi:secmeliYanlisCevapSayisi,toplamCozulmusSoruSayisi:secmeliToplamCozmusOlduguSoruSayisi,toplamSoruSayisi:secmeliToplamSoruSayisi}]
  end

  def ogrencininSorularaCevaplari
  
    dogruYanlisSorular = DogruYanlisQuestion.where(book_id: params[:book_id])
    secmeliSorular = SecmeliQuestion.where(book_id: params[:book_id])
  
    tfStudentAnswers = []
    tfQuestionAnswers = []
    secmeliStudentAnswers = []
    secmeliQuestionAnswers = []
    
    dogruYanlisSorular.each do |category|
        studentsoru = StudentsDyquestion.where(student_id: params[:student_id],dogru_yanlis_question_id: category.id)
        if !studentsoru.first.nil?
            soru = DogruYanlisQuestion.where(id: category.id)
            tfStudentAnswers+= studentsoru
            tfQuestionAnswers+= soru
            #bilgi+= '{soru_id:'+ category.id+',student_id:'+params[:student_id]+',book_id:'+params[:book_id]+',soruText:'+soru.first.soruText+',dogruCevap:'+soru.first.dogruCevap+',ogrenciCevap:'+studentsoru.first.cevap+'}'
        else
    
        end
    
    end
    
    secmeliSorular.each do |category|
        studentsoru = StudentsSecquestion.where(student_id: params[:student_id],secmeli_question_id: category.id)
        if !studentsoru.first.nil?
            soru = SecmeliQuestion.where(id: category.id)
            secmeliStudentAnswers += studentsoru
            secmeliQuestionAnswers += soru
            
        else
    
        end
    
    end
    

    render json: {dogruYanlisSoruBilgileri:tfQuestionAnswers,studentDogruYanlisSoruCevaplari:tfStudentAnswers,secmeliSoruBilgileri:secmeliQuestionAnswers,studentSecmeliSoruCevaplari:secmeliStudentAnswers}

  
  end
end