class Api::MessagesController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def create
    conversation = Conversation.find(params[:conversation_id])
    message=conversation.messages.new(message_params)
    
    if message.save 
      render json: message, status: 201 
    else
      render json: { errors: message.errors}, status: 422
    end
  end
  
  def index
    conversation = Conversation.find(params[:conversation_id])
    respond_with conversation.messages
  end
  
  def message_params
    params.permit(:id,:body,:sender)
  end
  
end
  
