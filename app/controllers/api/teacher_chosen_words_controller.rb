class Api::TeacherChosenWordsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    teacher = Teacher.find(params[:teacher_id])
    book = Book.find(params[:book_id])
    respond_with TeacherChosenWord.ogretmeninKitaptakiKelimeleri(teacher,book)
  end
  
  def show
    TeacherChosenWord.find(params[:id])
  end

end
  
