class Api::ConversationsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def create
    conversation =Conversation.new(conversation_params)
    conversation.sube_id = Student.find(params[:student_id]).sube_id
    if conversation.save 
      render json: conversation.id, status: 201 
    else
      render json: { errors: conversation.errors}, status: 422
    end
  end
  
  def index
    student = Student.find(params[:student_id])
    respond_with student.conversations
  end
  
  def conversation_params
    params.permit(:id,:student_id,:teacher_id,:konu)
  end
  
end
  
