class Api::VideoNotesController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    teacher = Teacher.find(params[:teacher_id])
    book = Book.find(params[:book_id])
    respond_with VideoNote.ogrencininKitaptakiVideoNotlari(teacher,book)
  end
  
  def show
   respond_with VideoNote.where(teacher_id: params[:teacher_id],book_id: params[:book_id],sayfaNo: params[:sayfaNo])
  end
end
  
