class Api::StudentsBooksController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def index
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    respond_with StudentsBook.ogrencininKitaptakiIstatistigi(student,book)
  end
  

  def ensonkalinansayfa
    student = Student.find(params[:student_id])
    book = Book.find(params[:book_id])
    
    record = StudentsBook.ogrencininKitaptakiIstatistigi(student,book)
    
    if record==nil
      record = StudentsBook.new(create_record_params)
      if record.save
        render json: record, status: 201 
      else
        render json: { errors: record.errors}, status: 422 
      end
    else
      if record.update(update_record_params)
        render json: record, status: 201 
      else
        render json: { errors: record.errors}, status: 422 
      end
    end
  end

  def update_record_params
    params.permit(:id,:istatistik1)
  end
  
  def create_record_params
    params.permit(:id,:student_id,:book_id,:istatistik1)
  end
  
end