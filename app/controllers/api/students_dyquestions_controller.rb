class Api::StudentsDyquestionsController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
  def create
    question =StudentsDyquestion.new(question_params)
    
    if question.save
        render json: @issue, status: 201 
    else
        render json: { errors: @issue.errors}, status: 422 
    end
  end

  def question_params
    params.permit(:id,:student_id,:dogru_yanlis_question_id,:cevap)
  end
  
  def show
   respond_with StudentsDyquestion.where(student_id: params[:student_id],dogru_yanlis_question_id: params[:id])
  end


end